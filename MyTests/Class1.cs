﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Threading.Tasks;
using Calculator;

namespace MyTests
{
    public class CalculatorTests
    {
        [Fact]
        public void Add_ShouldFindSum()
        {
            //Arrange
            double num1 = 5;
            double num2 = 6;
            double expected = 11;

            //Act
            TheCalculator calculator = new TheCalculator();
            double actual = calculator.Add(num1, num2);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Subtract_ShouldFindDifference()
        {
            double num1 = 4;
            double num2 = 2;
            double expected = 2;

            TheCalculator calculator = new TheCalculator();
            double actual = calculator.Subtract(num1, num2);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Multiply_ShouldFindProduct()
        {
            double num1 = 3;
            double num2 = 7;
            double expected = 21;

            TheCalculator calculator = new TheCalculator();
            double actual = calculator.Multiply(num1, num2);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Divide_ShouldFindQuotient()
        {
            double num1 = 8;
            double num2 = 2;
            double expected = 4;

            TheCalculator calculator = new TheCalculator();
            double actual = calculator.Divide(num1, num2);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Divide_IfBothNumbersAreNegative()
        {
            double num1 = -2;
            double num2 = -2;

            TheCalculator calculator = new TheCalculator();

            Assert.Throws<Exception>(() => calculator.Divide(num1, num2));
        }
        [Fact]
        public void Divide_IfDividedByZero()
        {
            double num1 = 8;
            double num2 = 0;

            TheCalculator calculator = new TheCalculator();

            Assert.Throws<Exception>(() => calculator.Divide(num1, num2));
        }
    }
}
